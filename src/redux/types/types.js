const types = {
  TOGGLE_THEME: 'TOGGLE_THEME',
  TOGGLE_SEARCH_INPUT: 'TOGGLE_SEARCH_INPUT',
  TOGGLE_NAV_BAR: 'TOGGLE_NAV_BAR',
  ADD_TO_BASKET: 'ADD_TO_BASKET',
  UPDATE_BASKET: 'UPDATE_BASKET',
  CHANGE_COUNT_BASKET: 'CHANGE_COUNT_BASKET',
  DELETE_BASKET_ITEM: 'DELETE_BASKET_ITEM',
  SET_USER: 'SET_USER',
  SIGN_UP_USER: 'SIGN_UP_USER',
  REMOVE_USER: 'REMOVE_USER',
  SET_INFORMATION: 'SET_INFORMATION',
  SET_SHIPPING: 'SET_SHIPPING',
  SET_PAYMENT: 'SET_PAYMENT',
  SUCCESSFUL_ORDER: 'SUCCESSFUL_ORDER',
  SET_TOKEN: 'SET_TOKEN',
  TOGGLE_PRODUCT_TO_CARD: 'TOGGLE_PRODUCT_TO_CARD',
  ADD_CATEGORY_TO_FILTER: 'ADD_CATEGORY_TO_FILTER',
  CLEAR_ALL_CATEGORIES_TO_FILTER: 'CLEAR_ALL_CATEGORIES',
  SET_ORDER_LIST: 'SET_ORDER_LIST',
  IS_OPEN_CART_WINDOW: 'IS_OPEN_CART_WINDOW',
  GET_PRODUCTS_PER_PAGE: 'GET_PRODUCTS_PER_PAGE',
  SET_PRICE_FILTER: 'SET_PRICE_FILTER',
  CLEAR_PRICE_FILTER: 'CLEAR_PRICE_FILTER',
  SET_SELECT_VALUE: 'SET_SELECT_VALUE',
  CLEAR_SELECT_VALUE: 'SET_SELECT_VALUE'
}
export const typesOfProducts = {
  GET_PRODUCT: 'GET_PRODUCT',
  GET_ALL_PRODUCTS: 'GET_ALL_PRODUCTS',
}

export default types